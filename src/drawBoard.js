import {canvasEdge} from './getConfigurations'
import {snakeWidth} from './getConfigurations'
import {forEach} from 'ramda'

const drawBoard = (ctx, snake, prey) => {
    ctx.clearRect(0, 0, canvasEdge, canvasEdge)
    ctx.fillStyle = '#222288'
    ctx.fillRect(0, 0, canvasEdge, canvasEdge)
    ctx.fillStyle = '#CC2222'
    ctx.fillRect(prey.x, prey.y, snakeWidth, snakeWidth)
    ctx.fillStyle = '#22CC22'
    forEach(e => ctx.fillRect(e.x, e.y, snakeWidth, snakeWidth), snake)
}

export default drawBoard