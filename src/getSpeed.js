import {initialSpeed} from './getConfigurations'

/**
 * @param {Boolean} increment
 * 
 */

const getSpeed = () => {
    const initial = initialSpeed 
    let speed = initial
    return (increment = false, reset = false) => {
        return speed = increment && speed > 30? speed - 3 : reset? initial : speed 
    }
}

export default getSpeed()