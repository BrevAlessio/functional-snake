/**
 * @description checks if object 1 and object 2, with th same width, are colliding ( overlapping )
 * @param {Object} object1 
 * @param {Object} object2 
 * @param {Number} width 
 */

export default (object1, object2, width) =>

    Math.abs(object1.x - object2.x) < width && Math.abs(object1.y - object2.y) < width


