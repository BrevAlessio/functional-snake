import {snakeWidth} from './getConfigurations'

/**
 * @description returns the next forseable head element of the snake
 * @param {Object} head 
 * @param {Object} direction 
 * @param {Number} edge
 * @returns {Object}
 */


export default (head, direction, edge) => {
    const newHead = {}

    for (const coordinate in head) {
        newHead[coordinate] = head[coordinate] + direction[coordinate]  >= edge? 
            0 : 
            head[coordinate] + direction[coordinate] < 0? 
                edge : 
                    head[coordinate] + direction[coordinate]
    }
    return newHead
}