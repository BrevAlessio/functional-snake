import {canvasEdge, snakeWidth} from './getConfigurations'
import {any, clone} from 'ramda'

/**
 * @param {Arrray} prey
 * 
 */

const getPrey = () => {
    let prey
    return (newPrey = false, snake = [{x: 0, y:0}]) => {
        if (newPrey || !prey){
            do {
                prey = {
                    x: Math.floor(Math.random() * Math.floor(canvasEdge / snakeWidth) ) * snakeWidth, 
                    y: Math.floor(Math.random() * Math.floor(canvasEdge / snakeWidth) ) * snakeWidth 
                }
                    
            } while (any( e => { prey.x === e.x && prey.y === e.y }, snake));
        }
        return prey
    }
}

export default getPrey()