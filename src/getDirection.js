import {snakeWidth} from './getConfigurations'

/**
 * @param {String} newDirection
 */

const getDirection = () => {

    const directions = {
        37: {x: -snakeWidth, y: 0},
        38: {x: 0, y: -snakeWidth},
        39: {x: snakeWidth, y: 0},
        40: {x: 0, y: snakeWidth}
    }
    let direction = 39
    return (newDirection = direction) =>{
        
        // can't go in opposite direction
        if (Math.abs(newDirection - direction) === 2) return directions[direction]

        return directions[direction = newDirection]
    }
}

export default getDirection()