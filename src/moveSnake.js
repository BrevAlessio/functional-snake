import { init, prepend } from 'ramda'

export default (snake, newHead) => prepend(newHead, init(snake))