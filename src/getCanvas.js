import {canvasEdge} from './getConfigurations'

/**
 * 
 * @param {String} selector
 * @returns {HTMLCanvasElement}
 */

const getCanvas = (selector) => {

    const canvas = document.querySelector(selector)
    canvas.height = canvasEdge
    canvas.width = canvasEdge

    return canvas
}

export default getCanvas