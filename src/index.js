import drawBoard from './drawBoard'
import getCanvas from './getCanvas'
import getContext from './getContext'
import getDirection from './getDirection'
import isGameOver from './isGameOver'
import getPrey from './getPrey'
import getPoints from './getPoints'
import getSnake from './getSnake'
import getSpeed from './getSpeed'
import isColliding from './isColliding'
import increaseSnake from './increaseSnake'
import moveSnake from './moveSnake'
import { keysBindings, snakeWidth, canvasEdge, selector, initialSnake } from './getConfigurations'
import getNewHead from './getNewHead';
import { any, head } from 'ramda'
import './scss/style.scss'

// config variables
// @todo bind time variables to animate function
let then = performance.now(),
    now = 0

// events listeners
addEventListener('keydown', e => {
    if (keysBindings.includes(e.keyCode) && !isGameOver()) {
        e.preventDefault()
        getDirection(e.keyCode)
    }
})

addEventListener('eat', () => {
    getSpeed(true)
    getPrey(true, getSnake())
    getPoints(false, false)
    document.querySelector('.points__value').textContent = getPoints()
})

addEventListener('gameover', () => {
    isGameOver(true)
    document.querySelector('.game-over').classList.toggle('hidden', false)
    getSpeed(false, true)
    getPoints(false, true)
})

document.querySelectorAll('.new-game').forEach(element => {    
    element.addEventListener('click', () => {
        isGameOver(false)
        document.querySelector('.points__value').textContent = '0'
        document.querySelector('.game-over').classList.toggle('hidden', true)
        getSnake(initialSnake)
        getPrey(true, getSnake())
    })
})

//game loop
function animate () {
        
    let then = performance.now(),
        now = 0
        
    return function animateClosure() {
        
        requestAnimationFrame(animateClosure)

        now = performance.now()
        let delta = now - then
                
        if ( delta < getSpeed() || isGameOver())
            return cancelAnimationFrame(animateClosure)
            
        const context = getContext(getCanvas(selector))
        const newHead = getNewHead( head(getSnake()), getDirection() ,canvasEdge)
        const isEating = isColliding(newHead, getPrey(), snakeWidth)
        const isEatingHimself = any( e => isColliding(newHead, e, snakeWidth), getSnake())

        if (isEating) {
            dispatchEvent(new Event('eat'))
            drawBoard( context, getSnake(increaseSnake(getSnake(), newHead)), getPrey() )
        } else if (isEatingHimself) {
            dispatchEvent(new Event('gameover'))
            drawBoard( context, getSnake(), getPrey() )
        } else{
            drawBoard( context, getSnake(moveSnake(getSnake(), newHead)), getPrey() )
        }

        then = now
    }
    
}

// Start application
animate()()
