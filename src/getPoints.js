import {increasePoints} from './getConfigurations'

export default ((increasePoints) => {
    let points = 0

    return (getter = true, reset = false) => points = getter? points : reset? 0 : points + increasePoints
})(increasePoints)