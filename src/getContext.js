const getContext = (canvas) => canvas.getContext('2d')

export default getContext