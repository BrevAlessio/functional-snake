const canvasEdge = 200
const initialSnake = [{x: 100, y: 100}]
const initialSpeed = 200
const keysBindings = [37, 38, 39, 40]
const increasePoints = 10
const snakeWidth = 20
const selector = '#board'

export {
    canvasEdge, 
    keysBindings, 
    increasePoints, 
    initialSnake, 
    initialSpeed, 
    selector, 
    snakeWidth, 
}