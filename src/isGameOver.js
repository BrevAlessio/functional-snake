export default (() => {
    let gameOver = false
    return (newStatus) => gameOver = newStatus === undefined? gameOver : newStatus 
})()