import {initialSnake} from './getConfigurations'
import {clone} from 'ramda'

/**
 * @param {Arrray} snake
 * 
 */

const getSnake = () => {
    let snake = clone(initialSnake)
    return (newSnake = snake) => {
        return snake = newSnake
    }
}

export default getSnake()