import {prepend} from 'ramda'

export default (snake, head) =>
    prepend(head, snake)
