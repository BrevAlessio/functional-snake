import isColliding from '../src/isColliding'
import {snakeWidth} from '../src/getConfigurations'

const mocks = [
    [{x: 1000, y: 1000}, {x: 50, y: 50}, false],
    [{x: 100, y: 100}, {x: 100, y: 100}, true],
]

mocks.forEach(
    (e, i) => test(`collision ${i} ${e[2]}`, () => {
        expect(isColliding(e[0], e[1], snakeWidth)).toBe(e[2])
    })
)
