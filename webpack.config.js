module.exports = {
    "mode": "development",
    "watch": true,
    "entry": __dirname+"/src/index.js",
    "output": {
        "path": __dirname+'/dist',
        "filename": "[name].js"
    },
    "module": {
        "rules": [
            {
                "test": /\.js$/,
                "exclude": /node_modules/,
                "use": {
                    "loader": "babel-loader",
                    "options": {
                        "presets": [
                            "env"
                        ]
                    }
                }
            },
            {
                "test": /\.scss$/,
                "use": [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    "watch": true
}